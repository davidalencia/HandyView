class vecManager {
  constructor(threshold, tetaThreshold, cb){
    this.threshold = threshold;
    this.tetaThreshold = tetaThreshold;
    this.cb = cb;
    this.lastVec = null;
    this.direction = null;
    this.currentVec = null;
  }
  addCoor(coor){
    if(this.currentVec == null){
      this.currentVec = coor;
      return;
    }

    var x = this.currentVec.x-coor.x;
    var y = this.currentVec.y-coor.y
    var m = (coor.y-this.currentVec.y)/(coor.x-this.currentVec.x);
    var teta = Math.asin(y);
    if(Math.abs(x)<this.threshold && Math.abs(y)<this.threshold)
      return;

    if(this.direction == null){
      this.direction = teta;
      return;
    }

    if(this.direction-teta<this.tetaThreshold){
      this.lastVec = {x, y};
      this.cb(hand:{
                position0: {
                  x:this.currentVec.x,
                  y:this.currentVec.y
                },
                position1:{
                  x:coor.x,
                  y:coor.y
                },
                vector:this.lastVec
              });
      this.currentVec = null;
      this.direction = null;
    }
  }
}

class infoManager {
  constructor(maxX, maxY, threshold, tetaThreshold, cb){
    this.maxX = maxX;
    this.maxY = maxY;
    this.threshold = threshold;
    this.tetaThreshold = tetaThreshold;
    this.cb = cb;
    this.left = new vecManager(threshold, tetaThreshold, info=>{
      this.leftInfo = info;
      this.cb(this.leftInfo, this.rightInfo, this.pose0, this.pose1);
      this.pose0 = this.pose1;
    });
    this.right = new vecManager(threshold, tetaThreshold, info=>{
      this.rightInfo = info;
      this.cb(this.leftInfo, this.rightInfo, this.pose0, this.pose1);
      this.pose0 = this.pose1;
    });
    this.leftInfo = null;
    this.rightInfo = null;
    this.pose0 = null;
    this.pose1 = null;
  }
  addPose(pose){
    for(var alfa=0; alfa<17;alfa++){
      pose[alfa].position.x = pose[alfa].position.x/this.maxX;
      pose[alfa].position.y = pose[alfa].position.y/this.maxY;
    }
    this.pose1 = pose;
    if(this.pose0 == null)
      this.pose0 = pose;
    var left = pose[9];
    var right = pose[10];
    if(left.score>0.4)
      this.left.addCoor(left.position);
    if(right.score>0.4)
      this.right.addCoor(right.position);
  }
}

async function main() {

const width=480
const height=360

const video =document.createElement("video");
const canvas = window.canvas = document.getElementById('canvi');
const ctx = canvas.getContext("2d");
canvas.width = width;
canvas.height = height;

const constraints = {
  video: {
    frameRate: {
      ideal: 20,
      max: 30
    }
  }
};
const loadConfig = {
  architecture: 'MobileNetV1',
  outputStride: 16,
  inputResolution: 257,
  multiplier: 0.5
};
const estimateConfig = {
  flipHorizontal: false,
  decodingMethod: 'single-person'
};

let predictions=0;
let net;
let pose=null;
const manager = new infoManager(width, height, 0.15, 0.1,
  (l, r, p0, p1)=>{
    console.log(l, r, p0, p1);
  });

//-------------------------------------------------------------------------
function drawCoor(coor, color) {
  ctx.beginPath()
  ctx.strokeStyle = color;
  ctx.lineWidth=3;
  ctx.arc(Math.floor(coor.x), Math.floor(coor.y), 10, 0, 2 * Math.PI);
  ctx.closePath();
  ctx.stroke();
}
function drawLine(c1, c2) {
  ctx.beginPath();
  ctx.strokeStyle = "#0dff00";
  ctx.moveTo(Math.floor(c1.x), Math.floor(c1.y));
  ctx.lineTo(Math.floor(c2.x), Math.floor(c2.y));
  ctx.stroke();
}

const handleError = e => console.error('navigator.getUserMedia error: ', e);

const load = () => posenet.load(loadConfig);

async function handleSuccess(stream) {
  video.srcObject = stream;
  video.play()
  drawToCanvas();
}

async function drawToCanvas() {
  ctx.drawImage(video , 0, 0, width, height);
  var left, right;
  if(pose!=null){
    manager.addPose(pose);
    left = pose[9];
    right = pose[10];
    if(left.score>0.4){
      // leftWrist.addCoor(left.position)
      drawCoor(left.position, "#0000FF");
    }
    if(right.score>0.4){
      // rightWrist.addCoor(right.position)
      drawCoor(right.position, "#ff0000");
    }
  }
  pose = (await net.estimatePoses(canvas, estimateConfig))[0].keypoints;
  predictions++;
  a = pose;
  requestAnimationFrame(drawToCanvas);
}

//------------------------------------------------------------------------
setInterval(()=>{
  $("#numPredictions").text(predictions);
  predictions=0;
}, 1000)


net = await load();

navigator.mediaDevices
  .getUserMedia(constraints)
  .then(handleSuccess)
  .catch(handleError);

}


main();
