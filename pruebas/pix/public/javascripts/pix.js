'use strict'
const hull = require('monotone-convex-hull-2d');

let a;
class Coor {
  constructor(x, y){
    this.x=x;
    this.y=y;
  }
}

async function main() {
  const width=480
  const height=360
  const constraints = { video: { frameRate: { ideal: 15, max: 30 } } };

  let outputStride = 16;
  let threshold = 0.7;
  let multiplier = 0.75;
  let predictions = 0;
  const video =$("<video></video>")[0];
  let canvas = $("#canvi")[0];
  let ctx = canvas.getContext("2d");
  canvas.width = width;
  canvas.height = height;

//------------------------------------------------------------------------------
  setInterval(()=>{
    $("#numPredictions").text(predictions);
    predictions=0;
  }, 1000);

  const load = m => bodyPix.load(m);

  const handleError = e => console.error('navigator.getUserMedia error: ', e);

  function drawCoor(coor, color) {
    ctx.beginPath()
    ctx.strokeStyle = color;
    ctx.arc(coor.x, coor.y, 1, 0, 2 * Math.PI);
    ctx.closePath();
    ctx.stroke();
  }

  function handleSuccess(stream) {
    video.srcObject = stream;
    video.play()
    drawToCanvas();
  }

  async function drawToCanvas() {
    var data = a=await net.estimatePartSegmentation(canvas,
                                                    outputStride,
                                                    threshold);
    var parts =data.data;
    let left = [], right = [];
    let leftX = 0, leftY = 0, rightX = 0, rightY = 0;
    let leftPoints = 0, rightPoints = 0;

    for (var alfa=0; alfa<parts.length; alfa++)
      if (parts[alfa]== 23)
        left.push(new Coor(alfa%data.width, Math.floor(alfa/data.width)))
      else if (parts[alfa]== 21)
        right.push(new Coor(alfa%data.width, Math.floor(alfa/data.width)))

    ctx.drawImage(video , 0, 0, width, height);
    left.forEach(coor=>{
      leftX += 1*coor.x;
      leftY += 1*coor.y;
      leftPoints++;
      drawCoor(coor, "rgb(0,0,255)")
    });
    console.log(`x:${leftX/leftPoints}, y:${leftY/leftPoints}`);
    right.forEach(coor => drawCoor(coor, "#FF0000"));

    predictions++;
    requestAnimationFrame(drawToCanvas);
  }


//------------------------------------------------------------------------------
  console.log("loading");
  const net = await load(multiplier);
  console.log("loaded");

  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(handleSuccess)
    .catch(handleError);


}

main();
