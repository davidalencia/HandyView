### 050619
Parece ser que pix logra identificar manos derecha e izquierda de manera adecuada.
Sin embargo es mas lento que pose-net pero nos da informacion que posnet no nos daba.
tenemos que ver que vale mas la pena la una posicion muy acertada y rapida de las
muñecas (posenet) o algo que englobe mejor a las manos aunque se tarde mucho mas
(bodypix).

### 240619
Dentro de pruebas/posenet esta un proyecto que se puede correr en el navegador
con el comando "npm start". Apoyado de posenet encontramos las muñecas y
descomponemos en vectores sus movimientos ademas de ilustrarlos en el navegador.
Creo que ya ahorita funciona bastante bien vecManager que es donde sucede la
magia de descomposición en vecctores, y el constructor de vecManager recibe una
función anónima que se ejecuta cada que se encuentra un vector.
posenet corre a unos 15 fps en Ubuntu en una nvidia gt 740m.
